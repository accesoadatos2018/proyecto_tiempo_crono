/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import static controlador.ControlDia.leerPrediccion;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import modelo.Dia;
import modelo.Prediccion;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Tomas
 */
public class ControlPrediccion extends ControlDom {

    static final String ET_ROOT = "root";
    File fichero = null;
    Prediccion p = null;

    public ControlPrediccion() {

        p = new Prediccion();
    }

    public ControlPrediccion(Prediccion p) {

        this.p = p;
    }

    public ControlPrediccion(Prediccion p, File f) {
        this.p = p;
        this.fichero = f;
    }

    public File getFichero() {
        return fichero;
    }

    public void setFichero(File fichero) {
        this.fichero = fichero;
    }

    public Prediccion getLibreria() {
        return p;
    }

    public void setLibreria(Prediccion libreria) {
        this.p = libreria;
    }

    public Document recuperar(File f) throws ParserConfigurationException, IOException, SAXException {

        Document documento;
        documento = deXmlaDocumento(f);
        documento.normalize();
        return documento;
    }

    public Prediccion leer(Document documento) {

        Element arrel;
        //NodeList lista;
        Element elementPrediccion;
        //Dia d;

        arrel = documento.getDocumentElement();

        elementPrediccion = obtenerElementoEtiqueta("prediccion", arrel);
        p = leerPrediccion(elementPrediccion); // <-- ESTO DEVUELVE UN DIA  
        //p.add(d);

        return p;
    }

    /*
    public Prediccion leer(Document documento) {

        Element arrel;
        Element elementPrediccion;
        NodeList listaDias;
        Dia d;

        arrel = documento.getDocumentElement();

        listaDias = arrel.getChildNodes();

        for (int i = 0; i < listaDias.getLength(); i++) {

            if (listaDias.item(i).getNodeType() == Node.ELEMENT_NODE) {
                d = leerPrediccion((Element) listaDias.item(i)); // <-- ESTO DEVUELVE UN DIA  
                p.add(d);
            }

        }

        return p;
    
     */
 /*
    public void escribir(Document d) {
        Element arrel;

        arrel = d.createElement(ET_ROOT);
        d.appendChild(arrel);

        for (Libro lib : l) {
            ControlLibro.escribirLibro(lib, arrel, d);
        }
    }*/
    public void guardar(Document documento) throws TransformerException {
        deDocaXml(documento, fichero);
    }

}
