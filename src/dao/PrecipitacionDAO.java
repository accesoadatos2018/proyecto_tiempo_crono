/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Dia;
import modelo.Precipitacion;

/**
 *
 * @author Tomas
 */
public class PrecipitacionDAO {
    
    public void inserta(Connection con, Precipitacion p, String fecha, int id) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("INSERT INTO probabilidad_precipitacion(id, periodo, probabilidad, fecha_dia) VALUES (?,?,?,?)");

            stmt.setInt(1, id);
            stmt.setString(2, p.getPeriodo());
            stmt.setInt(3, p.getProbabilidad());
            stmt.setString(4, fecha);

            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
    
    // METODO PARA ACTUALIZAR LA PRECIPITACIÓN DE UN DÍA
    public void actualizaPrecipitacion(Connection con, Precipitacion p, int id) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("UPDATE precipitacion SET probabilidad=? WHERE id=?");

                stmt.setInt(1, p.getProbabilidad());
                stmt.setInt(2, id);


            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PrecipitacionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
    
    // METODO PARA ELIMINAR UNA PRECIPITACIÓN
    public void elimina(Connection con, int id) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM precipitacion WHERE id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PrecipitacionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
    
}
