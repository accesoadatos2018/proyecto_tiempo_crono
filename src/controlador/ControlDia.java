/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import static controlador.ControlDom.obtenerElementoEtiqueta;
import static controlador.ControlDom.obtenerTextoEtiqueta;
import java.util.Date;
import modelo.Dia;
import modelo.Prediccion;
import modelo.Precipitacion;
import modelo.Temperatura;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Tomas
 */
public class ControlDia {

    static final String ET_PREDICCION = "prediccion";
    static final String ET_DIA = "dia";
    static final String ET_DATE = "fecha";
    static final String ET_PERIODO = "periodo";
    static final String ET_DESCRIPCION = "descripcion";
    static final String ET_Prop_prec = "prob_precipitacion";
    static final String ET_Estado_cielo = "estado_cielo";
    static final String ET_TEMPERATURA = "temperatura";
    static final String ET_MAXIMA = "maxima";
    static final String ET_MINIMA = "minima";

    public static Prediccion leerPrediccion(Element l) { /////////////////// <---- CAMBIAR NOM A leerDias

        Dia objetoDia = new Dia();

        Prediccion pred = new Prediccion();

        NodeList listaDias = l.getChildNodes();

        for (int i = 0; i < listaDias.getLength(); i++) {

            if (listaDias.item(i).getNodeType() == Node.ELEMENT_NODE) {
                objetoDia = leerDia((Element) listaDias.item(i));
                pred.add(objetoDia);
            }

        }

        return pred;
    }

    public static Dia leerDia(Element elementDia) {

        Element elementoDia;
        Element elementoEstado_Cielo;
        Element elementoTemperatura;
        Dia objetoDia = new Dia();

        String date = "";

        date = elementDia.getAttribute(ET_DATE);
        objetoDia.setDate(date);

        //elementoDia = obtenerElementoEtiqueta(ET_DIA, elementDia); // CREAMOS LISTA DE probabilidad_precipitacion
        leerProbabilidad(elementDia, objetoDia);

        elementoEstado_Cielo = obtenerElementoEtiqueta(ET_Estado_cielo, elementDia); // CREAMOS LISTA DE estado_cielo
        leerEstado_Cielo(elementoEstado_Cielo, objetoDia);

        elementoTemperatura = obtenerElementoEtiqueta(ET_TEMPERATURA, elementDia); // CREAMOS TEMPERATURA
        leerTemperatura(elementoTemperatura, objetoDia);

        return objetoDia;

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// LEER Y CREAR PROBABILIDAD DE PRECIPITACION 
    public static void leerProbabilidad(Element elementDia, Dia d) {

        int prob = 0;
        Precipitacion prob_prep;
        String periodo = ""; // ATRIBUTO DE prop_precipitacion y estado_cielo
        NodeList listaProp;
        Element prueba;
        int cont=0;

        //elementProb = obtenerElementoEtiqueta(ET_Prop_prec, elementDia);
        // CREAR CHILD NODES PER A PODER LLEGIR TOTS ELS QUE HI HAN
        listaProp = elementDia.getElementsByTagName(ET_Prop_prec);

        for (int i = 0; i < listaProp.getLength(); i++) {

            if (listaProp.item(i).getNodeType() == Node.ELEMENT_NODE) {

                prueba = (Element) listaProp.item(i);
                periodo = prueba.getAttribute(ET_PERIODO);
                
                if(periodo.equals("00-24")){
                    d.setPeriodo_Probabilidad_Precipitación(periodo);
                }
                
                

                prob = Integer.parseInt(listaProp.item(i).getTextContent());
                
                if(cont==0){
                    d.setProb_precipitacion(prob);
                    cont++;
                }

                prob_prep = new Precipitacion(periodo, prob);

                d.getListaProbabilidad().add(prob_prep);
                cont=0;

            }

        }

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// LEER Y CREAR ESTADO DEL CIELO 
    public static void leerEstado_Cielo(Element elementoEstado_Cielo, Dia d) {

        String periodo = ""; // ATRIBUTO DE prop_precipitacion y estado_cielo
        String descripcion = ""; // ATRIBUTO DE prop_precipitacion
        Element elementEstado = null;
        String estadoCielo = "";

        estadoCielo = elementoEstado_Cielo.getTextContent();
        periodo = elementoEstado_Cielo.getAttribute(ET_PERIODO);
        descripcion = elementoEstado_Cielo.getAttribute(ET_DESCRIPCION);

        d.setEstado_cielo(estadoCielo);
        d.setPeriodo_Estado_Cielo(periodo);
        d.setDescripcion(descripcion);

    }

    public static void leerTemperatura(Element l, Dia d) {

        Element elementoTempMax;
        Element elementoTempMin;
        Temperatura t = null;
        int max = 0;
        int min = 0;

        elementoTempMax = obtenerElementoEtiqueta(ET_MAXIMA, l);
        max = Integer.parseInt(obtenerTextoEtiqueta(ET_MAXIMA, l));

        elementoTempMin = obtenerElementoEtiqueta(ET_MINIMA, l);
        min = Integer.parseInt(obtenerTextoEtiqueta(ET_MINIMA, l));

        t = new Temperatura(max, min);
        d.setTemperatura(t);

    }

}
