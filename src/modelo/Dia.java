/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tomas
 */
public class Dia {

    private String date; // ATRIBUTO DE DIA
    private String periodo_Probabilidad_Precipitación;
    private int prob_precipitacion;
    private String periodo_Estado_Cielo; // ATRIBUTO PERIODO DE ESTADO CIELO
    private String descripcion; // ATRIBUTO DE ESTADO CIELO
    private String estado_cielo;
    private Temperatura temperatura;
    ArrayList<Precipitacion> listaProbabilidad;

    public Dia(String date, String periodo_prob_prec, String periodo_Estado_Cielo, String descripcion, int prob_precipitacion, String estado_cielo, Temperatura temperatura, ArrayList<Precipitacion> listaProbabilidad) {
        this.date = date;
        this.periodo_Probabilidad_Precipitación = periodo_prob_prec;
        this.periodo_Estado_Cielo = periodo_Estado_Cielo;
        this.descripcion = descripcion;
        this.prob_precipitacion = prob_precipitacion;
        this.estado_cielo = estado_cielo;
        this.temperatura = temperatura;
        this.listaProbabilidad = listaProbabilidad;

    }

    public Dia() {
        
        listaProbabilidad = new ArrayList();
    }

    public int getProb_precipitacion() {
        return prob_precipitacion;
    }

    public void setProb_precipitacion(int prob_precipitacion) {
        this.prob_precipitacion = prob_precipitacion;
    }

    public String getEstado_cielo() {
        return estado_cielo;
    }

    public void setEstado_cielo(String estado_cielo) {
        this.estado_cielo = estado_cielo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPeriodo_Estado_Cielo() {
        return periodo_Estado_Cielo;
    }

    public void setPeriodo_Estado_Cielo(String periodo_Estado_Cielo) {
        this.periodo_Estado_Cielo = periodo_Estado_Cielo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Temperatura getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Temperatura temperatura) {
        this.temperatura = temperatura;
    }

    public String getPeriodo_Probabilidad_Precipitación() {
        return periodo_Probabilidad_Precipitación;
    }

    public void setPeriodo_Probabilidad_Precipitación(String periodo_Probabilidad_Precipitación) {
        this.periodo_Probabilidad_Precipitación = periodo_Probabilidad_Precipitación;
    }

    public ArrayList<Precipitacion> getListaProbabilidad() {
        return listaProbabilidad;
    }

    public void setListaProbabilidad(ArrayList<Precipitacion> listaProbabilidad) {
        this.listaProbabilidad = listaProbabilidad;
    }

    @Override
    public String toString() {

        String resultado = "";

        resultado += "***** Dia *****" + "\n"
                + "Date: " + date + "\n";
        
        resultado += "***** Período Diário *****" + "\n"
                + "Periodo de Probabilidad de Precipitación: " + periodo_Probabilidad_Precipitación + "\n"
                + "Probabilidad de precipitación: " + prob_precipitacion + "\n"
                + "-----------------------------------------" + "\n";

        for (Precipitacion p : listaProbabilidad) {

            resultado += "Periodo de Probabilidad de Precipitación: " + p.getPeriodo() + "\n";
            resultado += "Probabilidad de precipitacion: " + String.valueOf(p.getProbabilidad()) + "\n";

        }

        resultado += "Periodo de Estado del Cielo: " + periodo_Estado_Cielo + "\n";
        resultado += "Descripcion: " + descripcion + "\n";
        resultado += "Estado del Cielo: " + estado_cielo + "\n";
        resultado += "***** Temperatura *****: " + "\n";
        resultado += temperatura.toString() + "\n";

        return resultado;

    }

}
