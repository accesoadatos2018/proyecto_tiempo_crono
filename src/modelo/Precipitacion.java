/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Tomas
 */
public class Precipitacion {
    
    private String periodo;
    private int probabilidad;
    
    public Precipitacion(){
        
    }

    public Precipitacion(String periodo, int probabilidad) {
        this.periodo = periodo;
        this.probabilidad = probabilidad;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getProbabilidad() {
        return probabilidad;
    }

    public void setProbabilidad(int probabilidad) {
        this.probabilidad = probabilidad;
    }

    @Override
    public String toString() {
        return "***** Probabilidad Precipitacion *****" + "\n" + 
               "Periodo: " + periodo + "\n" + 
               "Probabilidad: " + probabilidad + "\n";
    }
    
}
