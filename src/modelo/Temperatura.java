/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Tomas
 */
public class Temperatura {

    private int maxima;
    private int minima;

    public Temperatura(int maxima, int minima) {
        this.maxima = maxima;
        this.minima = minima;
    }

    public Temperatura() {

    }

    public int getMaxima() {
        return maxima;
    }

    public void setMaxima(int maxima) {
        this.maxima = maxima;
    }

    public int getMinima() {
        return minima;
    }

    public void setMinima(int minima) {
        this.minima = minima;
    }

    @Override
    public String toString() {
        return "Maxima: " + maxima + "\n"
                + "Minima: " + minima + "\n"; //<-- CUIDADO EN EL ULTIMO /n
    }

}
