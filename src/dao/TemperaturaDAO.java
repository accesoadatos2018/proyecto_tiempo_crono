/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Temperatura;

/**
 *
 * @author Tomas
 */
public class TemperaturaDAO {
    
    public void inserta(Connection con, Temperatura t, int id) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("INSERT INTO temperatura(id, maxima, minima) VALUES (?,?,?)");

            stmt.setInt(1, id);
            stmt.setInt(2, t.getMaxima());
            stmt.setInt(3, t.getMinima());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
    
    public void actualizaPrecipitacionMaxima(Connection con, Temperatura t, int id) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("UPDATE temperatura SET maxima=? WHERE id=?");

                stmt.setInt(1, t.getMaxima());
                stmt.setInt(2, id);


            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
    
    public void actualizaPrecipitacionMinima(Connection con, Temperatura t, int id) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("UPDATE temperatura SET minima=? WHERE id=?");

                stmt.setInt(1, t.getMinima());
                stmt.setInt(2, id);


            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
    
    // METODO PARA ELIMINAR UNA PRECIPITACIÓN
    public void elimina(Connection con, int id) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM temperatura WHERE id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TemperaturaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
        
}
