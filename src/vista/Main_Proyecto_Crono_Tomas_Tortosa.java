/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.ControlPrediccion;
import dao.Conexion_DB;
import dao.DiaDAO;
import dao.PrecipitacionDAO;
import dao.TemperaturaDAO;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;
import javax.xml.parsers.ParserConfigurationException;
import modelo.Dia;
import modelo.Precipitacion;
import modelo.Prediccion;
import modelo.Temperatura;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author Tomas
 */
public class Main_Proyecto_Crono_Tomas_Tortosa {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, SQLException {

        Scanner teclado = new Scanner(System.in);
        int opcion = 0;
        int opcionDia = 0;
        int opcionTemperatura = 0;
        int idBorrar = 0;

        String diaModificar = "";
        String descripcion = "";
        String estadoCielo = "";

        int idModificar = 0;
        int probabilidad = 0;

        int maximaModificar = 0;
        int minimaModificar = 0;

        String xml = "";
        File f;
        ControlPrediccion cr = null;
        Document d = null;
        Prediccion p = null;

        DiaDAO dDAO = new DiaDAO();
        PrecipitacionDAO pDAO = new PrecipitacionDAO();
        TemperaturaDAO tDAO = new TemperaturaDAO();

        Dia dia;

        Precipitacion prec;

        Temperatura t;

        // Abrimos conexión a base de datos nada más ejecutar la app.
        Conexion_DB conexion_DB = new Conexion_DB();
        System.out.println("Abrir conexión: ");
        Connection con = conexion_DB.AbrirConexion();
        System.out.println("Conexión Abierta Correctamente");

        do {

            menu();
            opcion = teclado.nextInt();

            switch (opcion) {

                case 1: // SELECCIONA FICHERO

                    xml = "aemet.xml";

                    f = new File(xml);

                    cr = new ControlPrediccion();

                    d = cr.recuperar(f);

                    System.out.println("DOCUMENTO CREADO CON ÉXITO!!");

                    break;

                case 2: // LEER DEL DOCUMENT Y CREAR BOOKSTORE

                    p = cr.leer(d);
                    System.out.println("Bookstore creado con éxito");

                    break;

                case 3: // MUESTRA EL OBJETO

                    System.out.println(p);

                    break;

                case 4: // INSERTAR EN BASE DE DATOS

                    for (Dia di : p) {

                        dDAO.inserta(con, di);
                        System.out.println("Hola");

                    }

                    break;

                case 5: // MODIFICAR DATO DIA

                    System.out.println("Dime el día que deseas modificar");
                    teclado.nextLine();
                    diaModificar = teclado.nextLine();

                    menuDia();
                    opcionDia = teclado.nextInt();

                    switch (opcionDia) {

                        case 1: // DESCRIPCION

                            System.out.println("Introduce la nueva descripción del día");
                            teclado.nextLine();
                            descripcion = teclado.nextLine();
                            dia = new Dia();

                            dia.setDate(diaModificar);
                            dia.setDescripcion(descripcion);

                            dDAO.actualizaDiaDescripcion(con, dia);

                            break;

                        case 2: // ESTADO DEL CIELO

                            System.out.println("Introduce el nuevo estado del cielo para el día");
                            teclado.nextLine();
                            estadoCielo = teclado.nextLine();
                            dia = new Dia();

                            dia.setDate(diaModificar);
                            dia.setEstado_cielo(estadoCielo);

                            dDAO.actualizaDiaEstadoCielo(con, dia);

                            break;

                        default:

                            System.out.println("Opción seleccionado NO válida");

                    }

                    break;

                case 6: // MODIFICAR PROBABILIDAD DE PRECIPITACION

                    System.out.println("Dime el id de la precipitación");
                    idModificar = teclado.nextInt();

                    System.out.println("Dime la nueva Probabilidad de Precipitación");
                    probabilidad = teclado.nextInt();

                    prec = new Precipitacion();

                    prec.setProbabilidad(probabilidad);

                    pDAO.actualizaPrecipitacion(con, prec, idModificar);

                    break;

                case 7: // MODIFICAR TEMPERATURA

                    System.out.println("Dime el id de la precipitación");
                    idModificar = teclado.nextInt();

                    menuTemperatura();
                    opcionTemperatura = teclado.nextInt();

                    switch (opcionTemperatura) {

                        case 1: // TEMPERATURA MÁXIMA

                            System.out.println("Dime la nueva Temperatura Máxima");
                            maximaModificar = teclado.nextInt();

                            t = new Temperatura();

                            t.setMaxima(maximaModificar);

                            tDAO.actualizaPrecipitacionMaxima(con, t, idModificar);

                            break;

                        case 2: // TEMPERATURA MÍNIMA

                            System.out.println("Dime la nueva Temperatura Minima");
                            minimaModificar = teclado.nextInt();

                            t = new Temperatura();

                            t.setMinima(minimaModificar);

                            tDAO.actualizaPrecipitacionMinima(con, t, idModificar);

                            break;

                        default:

                            System.out.println("Opcion seleccionada NO válida");

                            break;

                    }

                    break;

                case 8: // BORRAR DIA

                    System.out.println("Dime el día que deseas borrar");
                    teclado.nextLine();
                    diaModificar = teclado.nextLine();

                    dia = new Dia();

                    dia.setDate(diaModificar);

                    dDAO.elimina(con, dia);

                    break;

                case 9: // BORRAR PRECIPITACIÓN

                    System.out.println("Dime el id de la precipitación que quieres borrar");
                    idBorrar = teclado.nextInt();
                    
                    prec = new Precipitacion();
                    
                    pDAO.elimina(con, idBorrar);
                    
                    break;

                case 10: // BORRAR TEMPERATURA

                    System.out.println("Dime el id de la temperatura que quieres borrar");
                    idBorrar = teclado.nextInt();
                    
                    t = new Temperatura();
                    
                    tDAO.elimina(con, idBorrar);
                    
                    break;

            }

        } while (opcion != 11);

    }

    public static void menu() {
        System.out.println("1. Seleccionar fichero XML a recuperar y crear un document");
        System.out.println("2. Leer del document y crear un object Bookstore");
        System.out.println("3. Muestra el objeto");
        System.out.println("4. Insertar en Base de Datos");
        System.out.println("5. Modificar Día");
        System.out.println("6. Modificar Precipitación");
        System.out.println("7. Modificar Temperatura");
        System.out.println("8. Borrar Dia");
        System.out.println("9. Borrar Precipitación");
        System.out.println("10. Borrar Temperatura");
        System.out.println("11. Salir y Cerrar Conexión");
    }

    public static void menuDia() {

        System.out.println("Selecciona opción para cambiar");
        System.out.println("1. Descripción");
        System.out.println("2. Estado del cielo");

    }

    public static void menuTemperatura() {

        System.out.println("Selecciona opción para cambiar");
        System.out.println("1. Temperatura Màxima");
        System.out.println("2. Temperatura Mínima");

    }

}
