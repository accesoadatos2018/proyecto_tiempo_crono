/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Dia;
import modelo.Precipitacion;
import modelo.Temperatura;

/**
 *
 * @author Tomas
 */
public class DiaDAO {

    public void inserta(Connection con, Dia d) throws SQLException {

        PreparedStatement stmt = null;
        PreparedStatement stmt2 = null;
        PreparedStatement stmtPrecipitacion = null;
        ResultSet rs = null;
        TemperaturaDAO tDAO = new TemperaturaDAO();
        PrecipitacionDAO pDAO = new PrecipitacionDAO();
        int idTemperatura = 0;
        int idPrecipitacion = 0;

        try {

            // Insertamos temperatura antes que nada ya que para poder instertar un dia necesita parámetros de temperatura
            // Realizamos select al último id de Dia para incrementarlos de forma automática
            stmt2 = con.prepareStatement("SELECT id FROM temperatura");
            rs = stmt2.executeQuery();
            rs.next(); // Esta línea se utiliza para que no dé un error, lo que hacemos es incrementar a lo que apunta el resultado

            // Utilizamos bucle para quedarnos con el último id
            while (rs.next()) {
                idTemperatura = rs.getInt("id");
            }

            idTemperatura++;
            tDAO.inserta(con, d.getTemperatura(), idTemperatura);

            // Insertamos un DIA
            stmt = con.prepareStatement("INSERT INTO dia(fecha, periodo_estado_cielo, descripcion, estado_cielo, id_temperatura) VALUES (?,?,?,?,?)");

            stmt.setString(1, d.getDate());
            stmt.setString(2, d.getPeriodo_Estado_Cielo());
            stmt.setString(3, d.getDescripcion());
            stmt.setString(4, d.getEstado_cielo());
            stmt.setInt(5, idTemperatura);

            stmt.executeUpdate();

            // Realizamos select al último id para que los nuevos selects utilizen un id autonumérico a partir del último
            stmtPrecipitacion = con.prepareStatement("SELECT id FROM precipitacion");
            rs = stmtPrecipitacion.executeQuery();

            rs.next(); // Esta línea se utiliza para que no dé un error, lo que hacemos es incrementar a lo que apunta el resultado

            // Utilizamos bucle para quedarnos con el último id
            while (rs.next()) {
                idPrecipitacion = rs.getInt("id");
            }

            idPrecipitacion++;

            // Para cada día, añadimos todas las precipitaciones que tenga en el ArrayList
            for (Precipitacion p : d.getListaProbabilidad()) {

                pDAO.inserta(con, p, d.getDate(), idPrecipitacion);
                idPrecipitacion++;

            }

        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }

    // METODO PARA ACTUALIZAR LA DESCRIPCION DE UN DIA
    public void actualizaDiaDescripcion(Connection con, Dia d) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("UPDATE dia SET descripcion=? WHERE fecha=?");

                stmt.setString(1, d.getDescripcion());
                stmt.setString(2, d.getDate());


            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
    
    // METODO PARA ACTUALIZAR EL ESTADO DEL CIELO DE UN DIA
    public void actualizaDiaEstadoCielo(Connection con, Dia d) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("UPDATE dia SET estado_cielo=? WHERE fecha=?");

                stmt.setString(1, d.getEstado_cielo());
                stmt.setString(2, d.getDate());


            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }

    // METODO PARA BUSCAR UN DIA POR LA FECHA
    public Dia findbyFecha(Connection con, Dia d) throws SQLException {

        Dia dia = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement("SELECT * FROM dia WHERE fecha=?");
            stmt.setString(1, d.getDate());
            rs = stmt.executeQuery();

            while (rs.next()) {

                dia = new Dia();
                obtenerClienteFila(rs, dia);

            }

        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }

        return dia;

    }

    // METODO PARA OBTENER DATOS DE LA FILA DE UN CLIENTE
    public void obtenerClienteFila(ResultSet rs, Dia d) throws SQLException {

        d.setDate(rs.getString("fecha"));
        d.setDescripcion(rs.getString("descripcion"));
        d.setEstado_cielo(rs.getString("estado_cielo"));

    }

    // METODO PARA ELIMINAR UN DIA
    public void elimina(Connection con, Dia d) throws SQLException {

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM dia WHERE fecha=?");
            stmt.setString(1, d.getDate());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }
    
}
