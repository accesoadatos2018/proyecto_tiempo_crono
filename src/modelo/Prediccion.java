/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Tomas
 */
public class Prediccion extends ArrayList<Dia> {

    public ArrayList<Dia> getDias() {
        return this;
    }

    @Override
    public String toString() {
        String resultado = "";
        System.out.println("***** ROOT *****");
        for (int i = 0; i < this.getDias().size(); i++) {

            resultado += "\n" + this.getDias().get(i).toString();
        }

        return resultado;

    }
}
